
#!/bin/bash
#The applicatio requires postfix to be installed and configured.
#For more details on setting up a gmail client
# Visit https://www.linode.com/docs/email/postfix/configure-postfix-to-send-mail-using-gmail-and-google-apps-on-debian-or-ubuntu/
date=$(date)
FROM_ADDRESS="emailaddress"
SUBJECT="Gateway $status Alert"
BODY="Gateway $status since $date"
sendmail -oi -t -f "$FROM_ADDRESS" <<____HERE
From: Name <$FROM_ADDRESS>
To: email addresses separated by comma
Subject: $SUBJECT

$BODY
____HERE
